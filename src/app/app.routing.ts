import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { GridsComponent } from './components/grids/grids.component';
import { ChartsComponent } from './components/charts/charts.component';
import { PrincipalComponent } from './components/principal/principal.component';

const appRoutes: Routes = [
    {
        path: 'home', 
        component: HomeComponent,
        children: [
            {
                path: 'principal',
                component: PrincipalComponent
            },
            {
                path: 'grid',
                component: GridsComponent
            },
            {
                path: 'chart',
                component: ChartsComponent
            },
        ]
    },
    {
        path: '**',
        redirectTo: 'home/principal'
    }
];

export const Routing = RouterModule.forRoot(appRoutes);
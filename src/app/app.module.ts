import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';

import { MaterialModule } from './material.module';
import { CovalentModule } from './covalent.module';

import { PortalModule } from '@angular/cdk/portal';
import { Routing } from './app.routing';
import { MatIconRegistry } from '@angular/material';
import { HomeComponent } from './components/home/home.component';
import { ChartsComponent } from './components/charts/charts.component';
import { GridsComponent } from './components/grids/grids.component';
import { PrincipalComponent } from './components/principal/principal.component';
import { FloatFilterPipe } from './pipes/float-filter/float-filter.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TruncatePipe } from './pipes/truncate/truncate.pipe';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PrincipalComponent,
    ChartsComponent,
    GridsComponent,
    FloatFilterPipe,
    TruncatePipe
  ],
  imports: [
    BrowserModule,
    MaterialModule,
    CovalentModule,
    PortalModule,
    Routing,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
    
  ],
  providers: [MatIconRegistry],
  bootstrap: [AppComponent]
})
export class AppModule { }

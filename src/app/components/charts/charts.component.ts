import { Component, OnInit } from '@angular/core';
import { filter } from 'rxjs/operators';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IndicesService } from '../../services/indices/indices.service';
import * as moment from 'moment';
import { TdLoadingService } from '@covalent/core/loading';
import { BolsaService } from '../../services/bolsa/bolsa.service';
import { MatTableDataSource, MatPaginatorIntl, MatSnackBar } from '@angular/material';
import { TdChartComponent } from '@covalent/echarts/base';
import { TdDialogService } from '@covalent/core/dialogs';

import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD-MM-YYYY'
  },
  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'DD-MM-YYYY',
    monthYearA11yLabel: 'MMMM YYYY'
  }
};
@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css'],
  providers: [
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    { provide: MAT_DATE_LOCALE, useValue: 'es-CL' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] }
  ]
})
export class ChartsComponent implements OnInit {
  lastDateStart: any;
  lastDateFinish: any;
  dataSource: any;
  displayedColumns: string[];
  overlayStarSyntax: boolean;
  dataIndices: any;
  dataPeriodos: any;
  dataConfig: any;
  indiceForm: FormGroup;
  submitted: boolean;
  configTemp: any;
  config: any;
  viewChart: boolean;
  messages: any;
  componentData: any;

  constructor(
    private formBuilder: FormBuilder,
    private _dialogService: TdDialogService,
    private _bolsa: BolsaService,
    private _indices: IndicesService,
    private _loadingService: TdLoadingService,
    private _paginatorLabels: MatPaginatorIntl,
    private snackBar: MatSnackBar) {
    this.initilize();
  }

  snackBarMessage(message: string) {
    this.snackBar.open(message, 'OK', {
      duration: 3000
    });
  }

  initilize() {
    this.componentData = {
      title: "Gráficos",
      subtitle: "Seleccione el filtro para obtener los índices"
    }

    this.messages = {
      error: "Lo sentimos, hubo un inconveniente. Intentelo más tarde"
    };

    this.submitted = false;

    this._paginatorLabels.itemsPerPageLabel = "Items por página";
    this._paginatorLabels.nextPageLabel = "Página siguiente";
    this._paginatorLabels.firstPageLabel = "Primera página";
    this._paginatorLabels.lastPageLabel = "Última página";
    this._paginatorLabels.previousPageLabel = "Página anterior";

    this.indiceForm = this.formBuilder.group({
      indice: [1, Validators.required],
      periodo: ['ME', Validators.required],
      f_desde: ['2014-01-01', Validators.required],
      f_hasta: ['2014-12-31', Validators.required]
    });

    this.getIndices(1, 'ME', '2014-01-01', '2014-12-31');

    this.lastDateStart = new Date(moment("2015/01/01").format("YYYY-MM-DD"));
    this.lastDateFinish = this.lastDateStart;

    this._indices.getIndicesName().subscribe(res => {
      this.dataIndices = res['indicesCharts'];
      this.dataPeriodos = res['periodosCharts'];
    })

    this.viewChart = false;

    this.overlayStarSyntax = false;
  }

  ngOnInit() { }

  ngOnDestroy() {
    this._loadingService.resolve('overlayStarSyntax');
  }

  onSubmit() {
    this.submitted = true;

    if (this.indiceForm.invalid) {
      console.log(this.f.f_desde);
      return;
    }

    let indice = this.f.indice.value;
    let periodo = this.f.periodo.value;
    let f_desde = moment(this.f.f_desde.value).format("YYYY-MM-DD");
    let f_hasta = moment(this.f.f_hasta.value).format("YYYY-MM-DD");

    this.getIndices(indice, periodo, f_desde, f_hasta);
  }

  get f() { return this.indiceForm.controls; }


  getIndices(indice: number, periodo: string, f_desde: string, f_hasta: string) {
    let indiceSelected = this.getIndiceNames(indice);
    this._loadingService.register('overlayStarSyntax');
    this._bolsa.getIndices(indiceSelected, periodo, f_desde, f_hasta).subscribe(res => {
      this.createDataChart(res['indicesItem']);
      this._loadingService.resolve('overlayStarSyntax');
    }, err => {
      this._loadingService.resolve('overlayStarSyntax');
      this.snackBarMessage(this.messages.error);
    })
  }

  changeDate(date) {
    this.lastDateFinish = moment(date.value).format("YYYY-MM-DD");
  }


  //Create charts data

  createDataChart(indices: any) {
    this.getConfigData().then(res => {

      this.configTemp = "";
      this.configTemp = res;

      indices.forEach(element => {
        this.loadData(element);
      });

      this.viewChart = false;
      setTimeout(() => {
        this.viewChart = true;
      }, 100);

      this.config = {};
      this.config = this.configTemp;

    })
  }

  loadData(indiceItem) {
    let indices = this.formatData(indiceItem);

    this.configTemp['series'].filter(data => data.name === 'ind_act')[0].data.push(indices.ind_act[0]);
    this.configTemp['series'].filter(data => data.name === 'ind_ant')[0].data.push(indices.ind_ant[0]);
    this.configTemp['series'].filter(data => data.name === 'ind_may')[0].data.push(indices.ind_may[0]);
    this.configTemp['series'].filter(data => data.name === 'ind_med')[0].data.push(indices.ind_med[0]);
    this.configTemp['series'].filter(data => data.name === 'ind_men')[0].data.push(indices.ind_men[0]);
    this.configTemp['series'].filter(data => data.name === 'ind_pro')[0].data.push(indices.ind_pro[0]);
    this.configTemp['series'].filter(data => data.name === 'ind_var')[0].data.push(indices.ind_var[0]);

  }

  formatInd(num: number) {
    return num.toFixed(2);
  }


  formatData(indiceItem) {
    return {
      ind_act: [
        {
          name: indiceItem.fecha,
          value: [indiceItem.fecha, this.formatInd(indiceItem.ind_act)]
        }
      ],
      ind_ant: [
        {
          name: indiceItem.fecha,
          value: [indiceItem.fecha, this.formatInd(indiceItem.ind_ant)]
        }
      ],
      ind_may: [
        {
          name: indiceItem.fecha,
          value: [indiceItem.fecha, this.formatInd(indiceItem.ind_may)]
        }
      ],
      ind_med: [
        {
          name: indiceItem.fecha,
          value: [indiceItem.fecha, this.formatInd(indiceItem.ind_med)]
        }
      ],
      ind_men: [
        {
          name: indiceItem.fecha,
          value: [indiceItem.fecha, this.formatInd(indiceItem.ind_men)]
        }
      ],
      ind_pro: [
        {
          name: indiceItem.fecha,
          value: [indiceItem.fecha, this.formatInd(indiceItem.ind_pro)]
        }
      ],
      ind_var: [
        {
          name: indiceItem.fecha,
          value: [indiceItem.fecha, this.formatInd(indiceItem.ind_var)]
        }
      ]
    }
  }

  getConfigData() {
    return new Promise((resolve, reject) => {
      this._indices.getIndicesName().subscribe(res => {
        resolve(res['config']);
      })
    })
  }

  getIndiceNames(pos: number) {
    let indices = ["IGPA", "IPSA", "INTER-10"];
    return indices[pos - 1];
  }

}
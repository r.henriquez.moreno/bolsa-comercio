import { Component, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { TdMediaService } from '@covalent/core/media';
import { BolsaService } from 'src/app/services/bolsa/bolsa.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private _iconRegistry: MatIconRegistry,
    private _domSanitizer: DomSanitizer,
    public media: TdMediaService,
    private _bolsa: BolsaService) {
    this.getSvgIcons();

  }

  ngOnInit() {
  }

  getSvgIcons() {
    this._iconRegistry.addSvgIconInNamespace('assets', 'gitlab',
      this._domSanitizer.bypassSecurityTrustResourceUrl('../assets/icons/gitlab.svg'));
  }

  navmenu: Object[] = [
    {
      icon: 'home',
      route: 'principal',
      title: 'Home',
      description: 'Página principal'
    }, {
      icon: 'list',
      route: 'grid',
      title: 'Grilla',
      description: 'Vista general de la información'
    }, {
      icon: 'insert_chart',
      route: 'chart',
      title: 'Gráficos',
      description: 'IPSA, GPSA, INTER-10'
    }
  ];


}

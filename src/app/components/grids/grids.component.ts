import { Component, OnInit, ViewChild } from '@angular/core';
import { ITdDataTableColumn } from '@covalent/core/data-table';
import { TdDialogService } from '@covalent/core/dialogs';
import { BolsaService } from '../../services/bolsa/bolsa.service';
import { TdLoadingService } from '@covalent/core/loading';
import { MatPaginatorIntl, MatPaginator, MatTableDataSource, MatSnackBar } from '@angular/material';
import { IndicesService } from '../../services/indices/indices.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';

import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

export const MY_FORMATS = {
  parse: {
    dateInput: 'DD-MM-YYYY'
  },
  display: {
    dateInput: 'DD-MM-YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'DD-MM-YYYY',
    monthYearA11yLabel: 'MMMM YYYY'
  }
};

@Component({
  selector: 'app-grids',
  templateUrl: './grids.component.html',
  styleUrls: ['./grids.component.css'],
  providers: [
    { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS },
    { provide: MAT_DATE_LOCALE, useValue: 'es-CL' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] }
  ]
})
export class GridsComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  lastDateStart: any;
  lastDateFinish: any;
  dataSource: any;
  displayedColumns: string[];
  overlayStarSyntax: boolean;
  dataIndices: any;
  dataPeriodos: any;
  indiceForm: FormGroup;
  submitted: boolean;
  viewTable: boolean;
  messages: any;
  componentData: any;

  constructor(
    private formBuilder: FormBuilder,
    private _dialogService: TdDialogService,
    private _bolsa: BolsaService,
    private _indices: IndicesService,
    private _loadingService: TdLoadingService,
    private _paginatorLabels: MatPaginatorIntl,
    private snackBar: MatSnackBar) {

    this.initilize();

  }

  snackBarMessage(message: string) {
    this.snackBar.open(message, 'OK', {
      duration: 3000
    });
  }

  ngOnInit() { }

  ngOnDestroy() {
    this._loadingService.resolve('overlayStarSyntax');
  }

  initilize() {
    this.componentData = {
      title: "Grillas",
      subtitle: "Seleccione el filtro para obtener los índices"
    }

    this.messages = {
      error: "Lo sentimos, hubo un inconveniente. Intentelo más tarde"
    };

    this.indiceForm = this.formBuilder.group({
      indice: [1, Validators.required],
      periodo: ['ME', Validators.required],
      f_desde: ['2014-01-01', Validators.required],
      f_hasta: ['2014-12-31', Validators.required]
    });

    this.submitted = false;
    this.displayedColumns = ['indice', 'fecha', 'ind_act', 'ind_ant', 'ind_may', 'ind_men', 'ind_pro', 'ind_var'];

    this.getIndices(1, 'ME', '2014-01-01', '2014-12-31');

    this._paginatorLabels.itemsPerPageLabel = "Items por página";
    this._paginatorLabels.nextPageLabel = "Página siguiente";
    this._paginatorLabels.firstPageLabel = "Primera página";
    this._paginatorLabels.lastPageLabel = "Última página";
    this._paginatorLabels.previousPageLabel = "Página anterior";

    this.lastDateStart = new Date(moment("2015/01/01").format("YYYY-MM-DD"));
    this.lastDateFinish = this.lastDateStart;

    this._indices.getIndicesName().subscribe(res => {
      this.dataIndices = res['indices'];
      this.dataPeriodos = res['periodos'];
    })

    this.viewTable = false;

    this.overlayStarSyntax = false;

  }


  get f() { return this.indiceForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.indiceForm.invalid) {
      return;
    }

    let indice = this.f.indice.value;
    let periodo = this.f.periodo.value;
    let f_desde = moment(this.f.f_desde.value).format("YYYY-MM-DD");
    let f_hasta = moment(this.f.f_hasta.value).format("YYYY-MM-DD");

    this.getIndices(indice, periodo, f_desde, f_hasta);
  }

  getIndices(indice: number, periodo: string, f_desde: string, f_hasta: string) {
    let indiceSelected = this.getIndiceNames(indice);
    this._loadingService.register('overlayStarSyntax');
    this._bolsa.getIndices(indiceSelected, periodo, f_desde, f_hasta).subscribe(res => {
      this._loadingService.resolve('overlayStarSyntax');
      this.viewTable = true;
      this.dataSource = new MatTableDataSource<any>(res['indicesItem']);
      this.dataSource.paginator = this.paginator;
    }, err => {
      this._loadingService.resolve('overlayStarSyntax');
      this.snackBarMessage(this.messages.error);
      this.viewTable = false;
    })
  }

  changeDate(date) {
    this.lastDateFinish = moment(date.value).format("YYYY-MM-DD");
  }

  getIndiceNames(pos: number) {
    let indices = ["Todos los índices", "BANCA", "COM.&TEC.", "COMMODITIES", "CONST.&INMOB.", "CONSUMO", "IGPA", "IGPA LARGE", "IGPA MID", "IGPA SMALL", "INDUSTRIAL", "INTER-10", "IPSA", "RETAIL", "SALMON", "UTILITIES"];
    return indices[pos - 1];
  }
}


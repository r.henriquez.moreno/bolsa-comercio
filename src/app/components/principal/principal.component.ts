import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {

  features: any;
  componentData: any;

  constructor() {
    this.features = featureData;
    this.initialize();
  }

  ngOnInit(): void {
  }

  initialize(){
    this.componentData = {
      title: "Prueba técnica - Rodrigo Ignacio Henríquez Moreno",
      subtitle: "Postulación cargo:",
      subtitle_2: "analista programador"
      
    }

  }
}

const featureData = [
  {
    "icon": "list",
    "name": "Ejercicio 1 - Grilla",
    "description": "Una grilla que permita visualizar un índice seleccionable por el usuario, todos los índices, para un tipo de período y rango de fechas dados.",
    "route": "/home/grid"
  },
  {
    "icon": "insert_chart",
    "name": "Ejercicio 2 - Gráficos",
    "description": "Un gráfico de los índices IGPA, IPSA e INTER-10, en modalidad anual y mensual. Ambos parámetros seleccionables por el usuario.",
    "route": "/home/chart"
  }
];
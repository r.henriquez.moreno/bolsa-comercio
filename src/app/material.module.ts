import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule, MatNativeDateModule, MatInputModule, MatTabsModule, MatPaginatorModule, MatTableModule, MatTooltipModule } from '@angular/material';
import { MatIconModule } from '@angular/material/icon';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatRadioModule } from '@angular/material/radio';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';


@NgModule({
    imports: [
        BrowserAnimationsModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatNativeDateModule,
        MatInputModule,
        MatCardModule,
        MatRadioModule,
        MatIconModule,
        MatListModule,
        MatToolbarModule,
        MatGridListModule,
        MatTabsModule,
        MatPaginatorModule,
        MatTableModule,
        MatSnackBarModule,
        MatTooltipModule,
        MatMenuModule,
        MatSelectModule,
        MatSlideToggleModule
    ],
    exports: [
        BrowserAnimationsModule,
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatNativeDateModule,
        MatInputModule,
        MatCardModule,
        MatRadioModule,
        MatIconModule,
        MatListModule,
        MatToolbarModule,
        MatGridListModule,
        MatTabsModule,
        MatPaginatorModule,
        MatTableModule,
        MatSnackBarModule,
        MatTooltipModule,
        MatMenuModule,
        MatSelectModule,
        MatSlideToggleModule
    ],
})

export class MaterialModule { }
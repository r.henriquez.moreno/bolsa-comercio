import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class IndicesService {

  constructor(private http: HttpClient) { }

  getIndicesName() {
      return this.http.get("../assets/jsons/indices.json");
  }
}

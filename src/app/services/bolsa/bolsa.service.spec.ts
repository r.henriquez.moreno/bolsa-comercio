import { TestBed } from '@angular/core/testing';

import { BolsaService } from './bolsa.service';

describe('BolsaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BolsaService = TestBed.get(BolsaService);
    expect(service).toBeTruthy();
  });
});

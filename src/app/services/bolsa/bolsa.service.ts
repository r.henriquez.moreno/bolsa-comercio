import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { HttpParamsOptions } from '@angular/common/http/src/params';
import { CovalentHttpModule } from '@covalent/http';

@Injectable({
  providedIn: 'root'
})
export class BolsaService {

  private urlApi: string;

  constructor(public _http: HttpClient) {
    this.urlApi = "/bcstest/rest/indices/consultaIndices";
  }

  getIndices(indice: string, periodo: string, f_desde: string, f_hasta: string) {


    if (indice == "Todos los índices")
      indice = "";

    let params = new HttpParams().set('indice', indice).set('periodo', periodo).set('f_desde', f_desde).set('f_hasta', f_hasta);


    return this._http.get(this.urlApi, { params: params });

  }
}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'floatFilter'
})
export class FloatFilterPipe implements PipeTransform {

  transform(value: number, args?: any): any {
    return value.toFixed(2);
  }

}

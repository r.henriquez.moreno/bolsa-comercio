import { NgModule } from '@angular/core';

import { CovalentLayoutModule } from '@covalent/core/layout';
import { CovalentStepsModule } from '@covalent/core/steps';
/* any other core modules */
// (optional) Additional Covalent Modules imports
import { CovalentHighlightModule } from '@covalent/highlight';
import { CovalentMarkdownModule } from '@covalent/markdown';
import { CovalentDynamicFormsModule } from '@covalent/dynamic-forms';
import { CovalentMediaModule } from '@covalent/core/media';
import { CovalentFileModule } from '@covalent/core/file';
import { CovalentTabSelectModule } from '@covalent/core/tab-select';
import { CovalentLoadingModule } from '@covalent/core/loading';
import { CovalentDialogsModule } from '@covalent/core/dialogs';
import { CovalentBreadcrumbsModule } from '@covalent/core/breadcrumbs';
import { CovalentExpansionPanelModule } from '@covalent/core/expansion-panel';
import { CovalentJsonFormatterModule } from '@covalent/core/json-formatter';
import { CovalentHttpModule} from '@covalent/http';
import { CovalentDataTableModule } from '@covalent/core/data-table';


//eCharts

import { CovalentBaseEchartsModule } from '@covalent/echarts/base';
import { CovalentBarEchartsModule } from '@covalent/echarts/bar';
import { CovalentTooltipEchartsModule } from '@covalent/echarts/tooltip';
import { CovalentScatterEchartsModule } from '@covalent/echarts/scatter';
import { CovalentToolboxEchartsModule } from '@covalent/echarts/toolbox';
import { CovalentLineEchartsModule } from '@covalent/echarts/line';

@NgModule({
    imports: [
        CovalentLayoutModule,
        CovalentStepsModule,
        CovalentHighlightModule,
        CovalentMarkdownModule,
        CovalentDynamicFormsModule,
        CovalentMediaModule,
        CovalentLayoutModule,
        CovalentMediaModule,
        CovalentFileModule,
        CovalentTabSelectModule,
        CovalentLoadingModule,
        CovalentDialogsModule,
        CovalentBreadcrumbsModule,
        CovalentExpansionPanelModule,
        CovalentJsonFormatterModule,
        CovalentHttpModule,
        CovalentDataTableModule,
        CovalentBaseEchartsModule,
        CovalentBarEchartsModule,
        CovalentTooltipEchartsModule,
        CovalentScatterEchartsModule,
        CovalentToolboxEchartsModule,
        CovalentLineEchartsModule
    ],
    exports: [
        CovalentLayoutModule,
        CovalentStepsModule,
        CovalentHighlightModule,
        CovalentMarkdownModule,
        CovalentDynamicFormsModule,
        CovalentMediaModule,
        CovalentLayoutModule,
        CovalentMediaModule,
        CovalentFileModule,
        CovalentTabSelectModule,
        CovalentLoadingModule,
        CovalentDialogsModule,
        CovalentBreadcrumbsModule,
        CovalentExpansionPanelModule,
        CovalentJsonFormatterModule,
        CovalentHttpModule,
        CovalentDataTableModule,
        CovalentBaseEchartsModule,
        CovalentBarEchartsModule,
        CovalentTooltipEchartsModule,
        CovalentScatterEchartsModule,
        CovalentToolboxEchartsModule,
        CovalentLineEchartsModule
    ],
    declarations:[
    ]
})

export class CovalentModule { }